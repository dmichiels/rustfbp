use display;
use nanddisp;
use rustfbp::scheduler::Scheduler;
fn main() {
    let mut sched = Scheduler::new();

    let mut nanddisp = nanddisp::Agent::new(&mut sched);
    let disp = display::Agent::<bool, usize>::new(&mut sched);

    match disp.get(display::In::input(rustfbp::port::MsgSender::None)) {
        display::In::input(s) => nanddisp.connect(nanddisp::Connect::debug(s)),
        _ => unreachable!{},
    }

    match nanddisp.get(nanddisp::In::b(rustfbp::port::MsgSender::None)) {
        nanddisp::In::b(s) => s.h_send(true).unwrap(),
        _ => unreachable!{},
    }
    match nanddisp.get(nanddisp::In::debug(rustfbp::port::MsgSender::None)) {
        nanddisp::In::debug(s) => s.h_send(true).unwrap(),
        _ => unreachable!{},
    }

    nanddisp.add_sched(&mut sched);
    sched.add_agent(disp.get_id(), Box::new(disp));

    sched.run().unwrap();
}
