use rustfbp::{Input, Output, Agent};
use std::fmt::Display;

#[derive(Input)]
pub struct Input<T> {
    #[generic]
    pub input: MsgReceiver<T>,
    // For test
    pub a: MsgReceiver<bool>,
}

#[derive(Output)]
pub struct Output<T> {
    #[generic]
    pub output: MsgSender<T>,
    // For test
    pub res: MsgSender<String>,
}
#[derive(Agent)]
pub struct Agent<T, G> {
    pub input: Input<T>,
    pub output: Output<G>,
}

impl< T: Display + Into<G>, G> Handler for Agent<T, G> {
    fn run(&mut self) {
        let msg = self.input.input.recv().unwrap();
        println!("{}", msg);
        self.output.output.s_send(msg.into()).unwrap();
    }

}
