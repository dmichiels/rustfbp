use rustfbp::{Input, Output, Agent};

#[derive(Input)]
pub struct Input {
    pub a: MsgReceiver<bool>,
    pub b: MsgReceiver<bool>,
}

#[derive(Output)]
pub struct Output {
    pub res: MsgSender<bool>,
    pub error: MsgSender<String>,
}

#[derive(Agent)]
pub struct Agent {
    pub input: Input,
    pub output: Output,
}

impl Handler for Agent {
    fn run(&mut self) {
        let a = self.input.a.recv().unwrap();
        let b = self.input.b.recv().unwrap();

        let res = !(a && b);

        self.output.res.s_send(res).unwrap();
    }
}
