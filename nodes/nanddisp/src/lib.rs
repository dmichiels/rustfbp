use rustfbp::SubGraph;

#[derive(SubGraph)]
pub struct Agent {
    #[input(a, a)]
    #[input(b, b)]
    #[output(res, output)]
    pub nand: nand::Agent,
    #[input(debug, input, bool)]
    #[output(output, debug, bool)]
    pub disp: display::Agent<bool, bool>,
    pub disp2: display::Agent<String, String>,
}

impl Build for Agent {
    fn build(&mut self) {
        // Connect
        connect!(self, nand, res, input, disp);

        // Will fail to compile!
        // connect!(self, nand, res, input, disp2);

        // Mesg
        mesg!(self, nand, a, true)
    }
}

