pub mod port;
pub mod scheduler;

pub use rustfbp_agent::{Input, Output, Agent};
pub use rustfbp_graph::{SubGraph};

pub trait Handler {
    fn run(&mut self);
}

pub trait Build {
    fn build(&mut self);
}

#[macro_export]
macro_rules! connect {
    ($self:ident, $o:ident, $o_port:ident, $i_port:ident, $i:ident) => {
        match $self.$i.get(disp::In::$i_port(rustfbp::port::MsgSender::None)) {
            $i::In::$i_port(s) => $self.$o.connect(nand::Connect::$o_port(s)),
            _ => unreachable!{},
        }
    }
}

#[macro_export]
macro_rules! mesg {
    ($self:ident, $i:ident, $i_port:ident, $msg:expr) => {
        match $self.$i.get($i::In::$i_port(rustfbp::port::MsgSender::None)) {
            $i::In::$i_port(s) => s.h_send($msg).unwrap(),
            _ => unreachable!{},
        }
    }
}
