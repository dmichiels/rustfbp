use std::sync::mpsc::{SyncSender, Sender, Receiver, sync_channel};
use std::error::Error;
use crate::scheduler::{AgentMsg};

#[derive(Debug)]
pub enum MsgSender<T> {
    None,
    Some(MSender<T>),
}

impl<T> Clone for MsgSender<T> {
    fn clone(&self) -> MsgSender<T> {
        match self {
            MsgSender::None => MsgSender::None,
            MsgSender::Some(s) => MsgSender::Some(s.clone()),
        }
    }
}

impl<T> MsgSender<T> {
    pub fn s_send(&self, msg: T) -> Result<(), Box<dyn Error>> {
        if let MsgSender::Some(ref s) = self { s.send(msg) }
        else { Ok(()) }
    }

    pub fn h_send(&self, msg: T) -> Result<(), Box<dyn Error>> {
        if let MsgSender::Some(ref s) = self { s.send(msg) }
        else { Err("Not Connected".to_string())? }
    }

    pub fn my_clone(&self) -> MsgSender<T> {
        self.clone()
    }
}
#[derive(Debug)]
pub struct MSender<T> {
    sender: SyncSender<T>,
    dest: usize,
    sched: Sender<AgentMsg>,
}

impl<T> MSender<T> {
    pub fn send(&self, msg: T) -> Result<(), Box<dyn Error>> {
        self.sender.send(msg).or( Err("cannot send".to_string()))?;
        self.sched.send(AgentMsg::Inc(self.dest))?;
        Ok(())
    }
}

impl<T> Clone for MSender<T> {
    fn clone(&self) -> MSender<T> {
        MSender {
            sender: self.sender.clone(),
            dest: self.dest,
            sched: self.sched.clone(),
        }
    }
}

trait Connect {
    type T;

    fn connect(&mut self, sender: Self::T);
}

#[derive(Debug)]
pub struct MsgReceiver<T> {
    recv: Receiver<T>,
    pub sender: MsgSender<T>,
    pub id: usize,
    sched: Sender<AgentMsg>,
}

impl <T> MsgReceiver<T> {
    pub fn recv(&self) -> Result<T, Box<dyn Error>> {
        let msg = self.recv.recv()?;
        self.sched.send(AgentMsg::Dec(self.id))?;
        Ok(msg)
    }

    pub fn try_recv(&self) -> Result<T, Box<dyn Error>> {
        let msg = self.recv.try_recv()?;
        self.sched.send(AgentMsg::Dec(self.id))?;
        Ok(msg)
    }
}

pub fn msg_channel<T>(id: usize, sched: Sender<AgentMsg>) -> MsgReceiver<T> {
    let (s, r) = sync_channel(25);
    let msg_s = MsgSender::Some(MSender  {
        sender: s,
        dest: id,
        sched: sched.clone(),
    });
    let msg_r = MsgReceiver {
        recv: r,
        sender: msg_s,
        id: id,
        sched: sched,
    };
    msg_r
}
