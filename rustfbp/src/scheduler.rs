use crate::{Handler};

use std::sync::mpsc::{Sender, Receiver, channel};
use std::thread;
use std::error::Error;

pub enum AgentMsg {
    Inc(usize),
    Dec(usize),
    End(usize, Box<dyn Handler + Send>),
}

pub struct AgentState {
    agent: Option<Box<dyn Handler + Send>>,
    mesg: isize,
}

pub struct Scheduler {
    pub agents: Vec<AgentState>,
    pub sender: Sender<AgentMsg>,
    pub running: usize,
    recv: Receiver<AgentMsg>,
    id: usize,
}

impl Scheduler {
    pub fn new() -> Self {
        let (s, r) = channel();
        Self {
            agents: vec![],
            sender: s,
            running: 0,
            recv: r,
            id: 0,
        }
    }
    pub fn add_agent(&mut self, id: usize, agt: Box<Handler + Send>) {
        self.agents.insert(id, AgentState {
            agent: Some(agt),
            mesg: 0,
        })
    }

    pub fn reserve_id(&mut self) -> usize {
        self.id += 1;
        self.id - 1
    }

    pub fn run(&mut self) -> Result<(), Box<dyn Error>> {
        // TODO : Start all agents that have Initial Msg and no input port
        loop {
            match self.recv.recv()? {
                AgentMsg::Inc(id) => {
                    if let Some(agt) = self.agents.get_mut(id) {
                        agt.mesg += 1;
                        // Must run?
                        if agt.mesg > 0 {
                            if let Some(a) = agt.agent.take() {
                                self.run_agent(id, a)?;
                            }
                        }
                    } else {
                        return Err("Wrong id".into());
                    }
                },
                AgentMsg::Dec(id) => {
                    if let Some(agt) = self.agents.get_mut(id) {
                        agt.mesg -= 1;
                    } else {
                        return Err("Wrong id".into());
                    }
                },
                AgentMsg::End(id, agt) => {
                    // TODO: check for re-run
                    if let Some(a) = self.agents.get_mut(id) {
                        self.running -= 1;
                        if a.mesg > 0 {
                            self.run_agent(id, agt)?;
                        } else {
                            a.agent = Some(agt);
                            if self.running <= 0 {
                                break;
                            }
                        }
                    } else {
                        return Err("Wrong id".into());
                    }
                }
            }
        }
        Ok(())
    }

    fn run_agent(&mut self, id: usize, mut agt: Box<dyn Handler + Send>) -> Result<(), Box<dyn Error>> {
        let send_sched = self.sender.clone();
        self.running += 1;
        let _ = thread::spawn(move || {
            agt.run();
            send_sched.send(AgentMsg::End(id, agt)).expect("cannot send to the sched");
        });
        Ok(())
    }
}
