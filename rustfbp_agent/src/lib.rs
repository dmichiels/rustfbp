extern crate proc_macro;

use proc_macro2::TokenStream;
use quote::{quote, quote_spanned};
use syn::spanned::Spanned;
use syn::{parse_macro_input, parse_quote, Data, DeriveInput, Fields, GenericParam, Generics, Index};


#[proc_macro_derive(Input, attributes(generic))]
pub fn build_input(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let mut input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;

    let mut names = quote!{};
    let mut new_channel = quote!{};
    let mut last_name = quote!{};
    let mut get = quote!{};

    let mut types_in = quote!{};
    let mut enum_in = quote!{};

    match input.data {
        Data::Struct(ref mut data) => {
            match data.fields {
                Fields::Named(ref mut fields) => {
                    fields.named.iter_mut().for_each(|f| {
                        let name = &f.ident;
                        last_name = quote!{#name};
                        names = quote!{
                            #names
                            #name,
                        };
                        new_channel = quote!{
                            #new_channel
                            let #name = msg_channel(id, sched.sender.clone());
                        };
                        get = quote!{
                            #get
                            In::#name(_) => {In::#name(self.#name.sender.clone())},
                        };

                        let gen = syn::Ident::new(&format!("In_{}", name.as_ref().unwrap()), f.span());
                        let ty = &f.ty;
                        let arg = match ty {
                            syn::Type::Path(ref p) => {
                                &p.path.segments.iter().next().unwrap().arguments
                            },
                            _ => unimplemented!(),
                        };
                        if f.attrs.iter().count() > 0 {
                            types_in = quote!{
                                #types_in
                                pub type #gen<T> = MsgSender<T>;
                            };
                            enum_in = quote!{
                                #enum_in
                                #name(#gen #arg),
                            };
                        } else {
                            types_in = quote!{
                                #types_in
                                pub type #gen = MsgSender#arg;
                            };
                            enum_in = quote!{
                                #enum_in
                                #name(#gen),
                            };
                        }

                    });
                },
                _ => {},
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }

    let (impl_g, ty_g, where_cl) = &input.generics.split_for_impl();
    let expanded = quote!{
        use rustfbp::Handler;
        use rustfbp::port::{MsgReceiver, MsgSender, msg_channel};
        use rustfbp::scheduler::Scheduler;

        impl #impl_g #name #ty_g #where_cl {
            pub fn new(mut sched: &mut Scheduler) -> #name #ty_g {
                let id = sched.reserve_id();
                #new_channel
                #name {
                    #names
                }
            }

            pub fn get(&self, i: In#ty_g) -> In#ty_g {
                match i {
                    #get
                }
            }

            pub fn get_id(&self) -> usize {
                self.#last_name.id
            }
        }

        #types_in
        pub enum In#ty_g {
            #enum_in
        }

    };

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(Output, attributes(generic))]
pub fn build_output(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let mut input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;

    let mut new_channel = quote!{};
    let mut connect = quote!{};

    let mut types_out = quote!{};
    let mut enum_out = quote!{};

    match input.data {
        Data::Struct(ref mut data) => {
            match data.fields {
                Fields::Named(ref mut fields) => {
                    fields.named.iter_mut().for_each(|f| {
                        let name = &f.ident;
                        new_channel = quote!{
                            #new_channel
                            #name: MsgSender::None,
                        };
                        connect = quote!{
                            #connect
                            Connect::#name(s) => self.#name = s,
                        };

                        let gen = syn::Ident::new(&format!("Connect_{}", name.as_ref().unwrap()), f.span());
                        let ty = &f.ty;
                        let arg = match ty {
                            syn::Type::Path(ref p) => {
                                &p.path.segments.iter().next().unwrap().arguments
                            },
                            _ => unimplemented!(),
                        };
                        if f.attrs.iter().count() > 0 {
                            types_out = quote!{
                                #types_out
                                pub type #gen<T> = MsgSender<T>;
                            };
                            enum_out = quote!{
                                #enum_out
                                #name(#gen #arg),
                            };
                        } else {
                            types_out = quote!{
                                #types_out
                                pub type #gen = MsgSender#arg;
                            };
                            enum_out = quote!{
                                #enum_out
                                #name(#gen),
                            };
                        }

                    });
                },
                _ => {},
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }

    let (impl_g, ty_g, where_cl) = &input.generics.split_for_impl();
    let expanded = quote!{
        impl #impl_g #name #ty_g #where_cl {
            pub fn new() -> #name #ty_g {
                #name {
                    #new_channel
                }
            }

            pub fn connect(&mut self, c: Connect #ty_g) {
                match c {
                    #connect
                }
            }
        }

        #types_out
        pub enum Connect #ty_g {
            #enum_out
        }

    };

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(Agent)]
pub fn build_agent(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let mut input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let mut get = quote!{};
    let mut connect = quote!{};

    match input.data {
        Data::Struct(ref mut data) => {
            match data.fields {
                Fields::Named(ref mut fields) => {
                    fields.named.iter_mut().for_each(|f| {
                        let name = &f.ident;
                        let ty = &f.ty;
                        let arg = match ty {
                            syn::Type::Path(ref p) => {
                                &p.path.segments.iter().next().unwrap().arguments
                            },
                            _ => unimplemented!(),
                        };
                        if name.clone().unwrap().to_string() == "input" {
                            get = quote!{
                                pub fn get(&self, i: In#arg) -> In#arg {
                                    self.input.get(i)
                                }
                            }
                        } else {
                            connect = quote!{
                                pub fn connect(&mut self, c: Connect#arg) {
                                    self.output.connect(c);
                                }
                            }
                        }
                    })},
                _ => { unimplemented!() }
            }
        },
        _ => { unimplemented!() }
    }


    let (impl_g, ty_g, where_cl) = &input.generics.split_for_impl();
    let expanded = quote!{
        impl #impl_g #name #ty_g #where_cl {
            pub fn new(mut sched: &mut Scheduler) -> Self {
                Self {
                    input: Input::new(&mut sched),
                    output: Output::new(),
                }
            }

            pub fn add_sched(self, sched: &mut Scheduler) {}

            pub fn get_id(&self) -> usize {
                self.input.get_id()
            }

            #get

            #connect
        }

    };

    proc_macro::TokenStream::from(expanded)
}
