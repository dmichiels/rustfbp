extern crate proc_macro;

use proc_macro2::TokenStream;
use quote::{quote, quote_spanned};
use syn::spanned::Spanned;
use syn::{parse_macro_input, parse_quote, Data, DeriveInput, Fields, GenericParam, Generics, Index};


#[proc_macro_derive(SubGraph, attributes(input, output))]
pub fn build_subgraph(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let mut input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let mut uses = quote!{};
    let mut new = quote!{};
    let mut names = quote!{};
    let mut n_add_sched = quote!{};

    let mut type_in = quote!{};
    let mut enum_in = quote!{};
    let mut get = quote!{};

    let mut connect_out = quote!{};
    let mut enum_out = quote!{};
    let mut connect = quote!{};

    match input.data {
        Data::Struct(ref mut data) => {
            match data.fields {
                Fields::Named(ref mut fields) => {
                    fields.named.iter_mut().for_each(|f| {
                        let name = &f.ident;
                        names = quote!{
                            #names
                            #name,
                        };
                        let sp = &f.span();
                        f.attrs.iter()
                            .map(syn::Attribute::interpret_meta)
                            .for_each(|a| {
                                if let Some(a) = a {
                                    let io = a.name();
                                    if let syn::Meta::List(l) = a {
                                        let mut it = l.nested.iter();
                                        let a = it.next().map(|x| {
                                            if let syn::NestedMeta::Meta(syn::Meta::Word(i)) = x { i } else {unimplemented!()}
                                        }).unwrap();
                                        let b = it.next().map(|x| {
                                            if let syn::NestedMeta::Meta(syn::Meta::Word(i)) = x { i } else {unimplemented!()}
                                        }).unwrap();
                                        let c = it.next().map(|x|{
                                            if let syn::NestedMeta::Meta(syn::Meta::Word(i)) = x { i } else {unimplemented!()}
                                        });
                                        dbg!(name);
                                        dbg!(&io);
                                        dbg!(&a);
                                        dbg!(&b);
                                        let ty = if let Some(t) = c{
                                            quote!{<#t>}
                                        } else { quote!{} };
                                        if io == "input" {
                                            let gen_name = syn::Ident::new(&format!("In_{}", a), *sp);
                                            let gen_type = syn::Ident::new(&format!("In_{}", b), *sp);
                                            type_in = quote!{
                                                #type_in
                                                pub type #gen_name = #name::#gen_type#ty;
                                            };
                                            enum_in = quote!{
                                                #enum_in
                                                #a(#gen_name),
                                            };
                                            get = quote!{
                                                #get
                                                In::#a(_) => {
                                                    match self.#name.get(#name::In::#b(rustfbp::port::MsgSender::None)) {
                                                        #name::In::#b(s) => In::#a(s),
                                                        _ => unreachable!{},
                                                    }
                                                }
                                            };
                                        } else {
                                            let gen_name = syn::Ident::new(&format!("Connect_{}", b), *sp);
                                            let gen_type = syn::Ident::new(&format!("Connect_{}", a), *sp);
                                            connect_out = quote!{
                                                #connect_out
                                                pub type #gen_name = #name::#gen_type#ty;
                                            };
                                            enum_out = quote!{
                                                #enum_out
                                                #b(#gen_name),
                                            };
                                            connect = quote!{
                                                #connect
                                                Connect::#b(s) => self.#name.connect(#name::Connect::#a(s)),
                                            };

                                        };
                                    }
                                } else { unimplemented!() }
                            });
                        let ty = &mut f.ty;
                        match ty {
                            syn::Type::Path(ref mut p) => {
                                p.path.segments.iter_mut().for_each(|ref mut f| { f.arguments = syn::PathArguments::None });

                                let t = p.path.segments.iter().next().unwrap();

                                uses = quote!{
                                    #uses
                                    use #t as #name;
                                };
                                let a_n_add_sched = quote_spanned! {*sp =>
                                                                    sched.add_agent(self.#name.get_id(), Box::new(self.#name));
                                };
                                n_add_sched = quote!{#n_add_sched #a_n_add_sched};
                            }
                            _ => unimplemented!()
                        }
                        let a_new = quote_spanned! {*sp=>
                                                    let #name = #ty::new(&mut sched);
                        };
                        new = quote!{
                            #new #a_new
                        };

                    });
                },
                _ => {},
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }

    let (impl_g, ty_g, where_cl) = &input.generics.split_for_impl();
    let expanded = quote!{
        use rustfbp::scheduler::Scheduler;
        use rustfbp::{Build, connect, mesg};
        #uses
        impl #impl_g #name #ty_g #where_cl {
            pub fn new(mut sched: &mut Scheduler) -> #name #ty_g {
                #new

                let mut x = #name {
                    #names
                };
                x.build();
                x
            }

            pub fn add_sched(self, sched: &mut Scheduler) {
                #n_add_sched
            }

            pub fn get(&self, i: In) -> In {
                match i {
                    #get
                }
            }

            pub fn connect(&mut self, c: Connect) {
                match c {
                    #connect
                }
            }
        }

        #type_in
        pub enum In {
            #enum_in
        }

        #connect_out
        pub enum Connect {
            #enum_out
        }
    };

    proc_macro::TokenStream::from(expanded)
}
